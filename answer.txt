1. Функція створюється за допомогою ключового слова function.
Приклад:
function userAge(){
    console.log(25);
}
А викликається вона при написані назви функції.
Приклад:
userAge();

2.Оператор return повертає значення функції і припиняє її виконання.
Приклад:
function userAge(){
    let age = 25;
    return age;
}

console.log(userAge);

3.Параметри - це зміні, які оголошеніі в об'яві функції. Аргуенти - це конкретні значення, передані функції.
Приклад:
function userAge(age){
    console.log(age);
}

4. Приклад:
function userName(name){
    return name;
}

function sayHello(userName){
    console.log("Привіт " + userName);
}

sayHello(userName("Марія"));