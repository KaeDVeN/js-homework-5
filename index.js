"use strict"
let number1;
let number2;
let mathSymbol;

do { //цикл для перевірки чисел
    number1 = +prompt("Введіть 1-ше число");
    number2 = +prompt("Введіть 2-ге число");

    if(isNaN(number1) || isNaN(number2)){
        alert('Два або одне із чисел не є числом, введіть ще раз');
    }
}while (isNaN(number1) || isNaN(number2));

do { //цикл для перевірки математичної операції
    mathSymbol = prompt("Введіть яку математичну операцію ви хочете здійснити");

    if (mathSymbol !== "+" && mathSymbol !== "-" && mathSymbol !== "*" && mathSymbol !== "/"){
        alert('Такої математичної операції не існує');
    }
}while(mathSymbol !== "+" && mathSymbol !== "-" && mathSymbol !== "*" && mathSymbol !== "/");

function math(number1, number2, mathSymbol){
    if (mathSymbol === '+'){
        return number1 + number2;
    }else if (mathSymbol === '-'){
        return number1 - number2;
    }else if (mathSymbol === '*'){
        return number1 * number2;
    }else{
        return number1 / number2;
    }
}

console.log(math(number1, number2, mathSymbol));



//3-тє завдання

// let factorialNumber;
// do{
//     factorialNumber = prompt("Введіть число з якого хочете отримати факторіал")
//
//     if (isNaN(factorialNumber)){
//         alert('Ви ввели не число, введіть ще раз');
//     }
// }while (isNaN(factorialNumber));
//
// function factorialOperation(factorialNumber){
//     let factorialResult = factorialNumber;
//     for (let i = --factorialNumber; i > 0; i--){
//         factorialResult *= i;
//     }
//     return factorialResult;
// }
//
// alert(factorialOperation(factorialNumber));